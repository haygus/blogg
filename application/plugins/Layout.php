<?php

class Plugin_Layout extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
    	
        if ('admin' == $request->getModuleName() && Zend_Auth::getInstance()->hasIdentity()) {
            
            // Change layout
            Zend_Layout::getMvcInstance()->setLayout('layout_admin');
            
        }

        return;
    }
}