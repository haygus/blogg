<?php
/**
 * class Form_Login extends Zend_Form
 * @author Haygus
 * 
 */

class Form_Login extends Zend_Form
{
    public function __construct($options = null)
    {
        parent::__construct($options);

        $this->setName('login');

        $nickname = new Zend_Form_Element_Text('nickname');
        $nickname->setLabel('nickname')
                        ->setDescription('3-255 characters')
                        ->setRequired(true)
                        ->setAttrib('tabindex',10)
                        ->setAttrib('accesskey', '1')
                        ->addFilter('StripTags')
                        ->addFilter('StringTrim')
                        ->removeDecorator('DtDdWrapper')
                        ->addValidator('StringLength', false, 3, 255)
                        ->addValidator('NotEmpty');

        $password = new Zend_Form_Element_Password('password');
        $password->setLabel('password')
                        ->setDescription('3-200 characters')
                        ->setRequired(true)
                        ->setAttrib('tabindex', 20)
                        ->setAttrib('accesskey', 2)
                        ->addFilter('StripTags')
                        ->addFilter('StringTrim')
                        ->addValidator('StringLength', false, 3, 200)
                        ->addValidator('NotEmpty');	


        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('submit');

        $this->addElements(array($nickname, $password, $submit));
    }
        
}