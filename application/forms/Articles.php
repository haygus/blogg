<?php
/**
 * class Form_Articles extends Zend_Form
 * @author Haygus
 * 
 */

class Form_Articles extends Zend_Form
{
    public function __construct($options = null)
    {
        parent::__construct($options);
        
        $this->setName('article');
        
        /* Elements */
        $idArticle = new Zend_Form_Element_Hidden('id_article');
        
        $titre = new Zend_Form_Element_Text('titre');
        $titre->setLabel('titre')
                        ->setRequired(true)
                        ->setAttrib('tabindex',10)
                        ->setAttrib('accesskey', '1')
                        ->addFilter('StringTrim')
                        ->removeDecorator('DtDdWrapper')
                        ->addValidator('StringLength', false, 1, 255)
                        ->addValidator('NotEmpty');
        
        $contenu = new Zend_Form_Element_Textarea('contenu');
        $contenu->setLabel('contenu')
                        ->setRequired(true)
                        ->setAttrib('tabindex',20)
                        ->setAttrib('accesskey', '2')
                        ->removeDecorator('DtDdWrapper')
                        ->addValidator('NotEmpty');
        
        $online = new Zend_Form_Element_Checkbox('online');
        $online->setLabel('online')
                        ->setOptions(array(1 => 'Oui'));
        
        $date_published = new Zend_Form_Element_Text('date_published');
        $date_published->setLabel('date_published')
                        ->setValue('0000-00-00 00:00:00');
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setLabel('submit');

        $this->addElements(array($idArticle, $titre, $contenu, $online, $date_published, $submit));
    }
        
}