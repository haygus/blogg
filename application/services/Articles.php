<?php

/**
 * Service URLs
 * @version 0.1
 * @author Haygus
 */

class Service_Articles extends App_ServiceAbstract
{
    /**
     * L'instance du mapper Tags
     * @access protected
     * @var Model_Mapper_Tags
     */
    protected $_mapperTags;
    
    /**
     * Le service des Tags
     * @access protected
     * @var Service_Tags
     */
    protected $_serviceTags;
    
    /**
     * Init for Articles
     * @access public
     */

    public function init()
    {
        $this->_mapper = new Model_Mapper_Articles();
        $this->_serviceTags = Service_Tags::getInstance();
        $this->_collectionClass = 'Model_Collection_Articles';
        $this->_domainClass = 'Model_Object_Articles';
    }

    /**
     * Fetch all Articles
     * @access public
     * @return Model_Collection_Articles
     */

    public function fetchAll($options = array())
    {
        
        $articles = $this->_mapper->fetchAll($options);

        // Get Tags
        $articles = $this->_serviceTags->buildRelatedsLinks($articles, 'Model_DbTable_TagsArticles');
        
        
        return $this->_createCollection($articles);
    }
    
    /**
     *  Fetch All Articles by Tag
     * @access public
     * @return Model_Collection_Articles
     */
    public function fetchByTags(array $tags)
    {
        if (!empty($tags)) {
            
            $idTags = array();
            $tags = $this->_serviceTags->fetchByLabels($tags);
            
            // on créer le tableau avec juste les id_tag
            foreach ($tags as $tag) {
                $idTags[] = $tag['id_tag'];
            }
            
            $articles = $this->_mapper->fetchByTags($idTags);
            
            $articles = $this->_serviceTags->buildRelatedsLinks($articles, 'Model_DbTable_TagsArticles');
        } else {
            throw new Exception('Need tag');
        }
        
        return $this->_createCollection($articles);
    }
    
    
    /**
     * Fetch an article by a token
     * 
     * @access public
     * @param string $token
     * @throws App_Model_Mapper_RuntimeException
     * 
     * @return Model_Object_Articles
     */
    public function fetchByToken($token)
    {
        $article = $this->_mapper->fetchByToken($token);
        
        if (!$article) {
            throw new App_Model_Mapper_RuntimeException('Cet article n\'existe pas', 404);
        }
        
        $article = $this->_buildArticle($article);
        
        return $this->_createObject($article);
    }
    /**
     * Fetch an article by his identifier
     * 
     * @access public
     * @param int $id_article
     * @throws App_Model_Mapper_RuntimeException
     * 
     * @return Model_Object_Articles
     */
    public function fetchById($id_article)
    {
    	$id_article = (int) $id_article; 
    	
    	$article = $this->_mapper->fetchById($id_article);
    	
    	if (!$article) {
    		throw new App_Model_Mapper_RuntimeException('Cet article n\'existe pas', 404);
    	}
    	
    	$article = $this->_buildArticle($article);
    	
    	return $this->_createObject($article);
    }
    
    /**
     * Build article, add tags
     * 
     * @param array $article
     * @access proctected
     * 
     * @return array
     */
    protected function _buildArticle(array $article)
    {
    	$articles = array($article);
    	
    	// Get Tags
    	$articles = $this->_serviceTags->buildRelatedsLinks($articles, 'Model_DbTable_TagsArticles');
    	
    	return $articles[0];
    }
}