<?php

/**
 * Service URLs
 * @version 0.1
 * @author Haygus
 */

class Service_Urls extends App_ServiceAbstract
{
    /**
     * L'instance du mapper Tags
     * @access protected
     * @var Model_Mapper_Tags
     */
    protected $_mapperTags;
    
    /**
     * Le service des Tags
     * @access protected
     * @var Service_Tags
     */
    protected $_serviceTags;
    
    /**
     * Init for URLs
     * @access public
     */

    public function init()
    {
        $this->_mapper = new Model_Mapper_Urls();
        $this->_serviceTags = Service_Tags::getInstance();
        $this->_collectionClass = 'Model_Collection_Urls';
        $this->_domainClass = 'Model_Object_Urls';
    }

    /**
     * Fetch all URLs
     * @access public
     * 
     * @param offset integer
     * @param limit integer
     * @param tag string
     * @return Model_Collection_URLs
     */

    public function fetchAll($offset = NULL, $limit = NULL, $tag = NULL)
    {
        // Options
        $options = array();
        
        if ($offset !== NULL) {
            $options['offset'] = $offset;
        }
        
        if ($limit !== NULL) {
            $options['limit'] = $offset;
        }
        
        if ($tag !== NULL) {
            $options['tag'] = $tag;
        }
        
        $urls = $this->_mapper->fetchAll($options);
        
        // Get Tags
        $urls = $this->_serviceTags->buildRelatedsLinks($urls, 'Model_DbTable_TagsUrls');
        
        
        return $this->_createCollection($urls);
    }
    
    
}