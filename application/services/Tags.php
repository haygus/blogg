<?php

/**
 * Service URLs
 * @version 0.1
 * @author Haygus
 */

class Service_Tags extends App_ServiceAbstract
{


    /**
     * Init mappers for URLs
     * @access public
     */

    public function init()
    {
        $this->_mapper = new Model_Mapper_Tags();
        $this->_collectionClass = 'Model_Collection_Tags';
        $this->_domainClass = 'Model_Object_Tags';
    }

    /**
     * Fetch all URLs
     * @access public
     * @return Model_Collection_URLs
     */

    public function fetchAll($offset = NULL, $limit = NULL)
    {
        // Options
        $options = array();
        
        if ($offset !== NULL) {
            $options['offset'] = $offset;
        }
        
        if ($limit !== NULL) {
            $options['limit'] = $offset;
        }
        
        
        $tags = $this->_mapper->fetchAll($options);
        
        
        
        
        return $this->_createCollection($tags);
        
    }
    
    /**
     * Retourne les tags selon un tableau d'id_url
     */
    
    protected function _fetchByRelated($relateds, $DbTableName)
    {
        
        $tags = $this->_mapper->fetchByRelated($relateds, $DbTableName);
        
        
        return $this->_createCollection($tags);
    }
    
    /**
     * Retourne les liaisons
     * @access protected
     * @param array $relateds 
     * @param string $DbTableName
     * @return array
     */
    protected function _getTagsByRelateds($relateds, $DbTableName)
    {
        $tags = $this->_mapper->getTagsByRelateds($relateds, $DbTableName);
        
        return $tags;
    }
    
    
    /**
     * Créer les liens entre les tags et les entités
     * @param array $relateds
     * @param string $DbTableName
     * @return type 
     */
    
    public function buildRelatedsLinks($relateds, $DbTableName)
    {
        // Tous les tags concernés par les liaisons
        $tags = $this->_fetchByRelated($relateds, $DbTableName);
        
        // On recup les liaisons
        $tagsRelateds = $this->_getTagsByRelateds($relateds, $DbTableName);
        
        
        $relateds = $this->_mapper->buildRelatedsLinks($relateds, $DbTableName, $tags, $tagsRelateds);
        
        
        return $relateds;
    }
    
    /**
     * Retourne les id des tags demandé
     * @param array $tags
     * @return array
     */
    public function fetchByLabels(array $tags)
    {
        $tags = $this->_mapper->fetchByLabels($tags);
        
        return $tags;
    }
}