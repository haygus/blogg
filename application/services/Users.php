<?php

/**
 * Service URLs
 * @version 0.1
 * @author Haygus
 */

class Service_Users extends App_ServiceAbstract
{

    
    /**
     * Le service des Tags
     * @access protected
     * @var Service_Tags
     */
    protected $_serviceUsers;
    
    /**
     * Init for Users
     * @access public
     */

    public function init()
    {
        $this->_mapper = new Model_Mapper_Users();
        $this->_collectionClass = 'Model_Collection_Users';
        $this->_domainClass = 'Model_Object_Users';
    }

    /**
     * Fetch all Users
     * @access public
     * @return Model_Collection_Users
     */

    public function fetchAll($offset = NULL, $limit = NULL)
    {
        // Options
        $options = array();
        
        if ($offset !== NULL) {
            $options['offset'] = $offset;
        }
        
        if ($limit !== NULL) {
            $options['limit'] = $offset;
        }
        
        $users = $this->_mapper->fetchAll($options);
        

        // Get Tags
        $users = $this->_serviceTags->buildRelatedsLinks($users, 'Model_DbTable_TagsUsers');
        
        
        return $this->_createCollection($users);
    }
    /**
     * Fetch All Users by Tag
     * @access public
     * @return Model_Collection_Users
     */
    public function fetchByTags(array $tags)
    {
        
        if (!empty($tags)) {
            
            $idTags = array();
            $tags = $this->_serviceTags->fetchByLabels($tags);
            
            // on créer le tableau avec just les id_tag
            foreach ($tags as $tag) {
                $idTags[] = $tag['id_tag'];
            }
            
            
            
            $users = $this->_mapper->fetchByTags($idTags);
            
            
            $users = $this->_serviceTags->buildRelatedsLinks($users, 'Model_DbTable_TagsUsers');
        } else {
            throw new Exception('Need tag');
        }
        
        return $this->_createCollection($users);
    }
    
    

}