<?php

class Admin_TagsController extends App_Controller_Admin
{
    /**
     * Le service Tags
     * @var Service_Tags
     */
    protected $_tags;
    
    /**
     * Init pour l'UI de l'admin
     */
    public function initAdmin()
    {
        $this->_tags = Service_Tags::getInstance();
    }

    public function indexAction()
    {
        // collection articles
        $this->view->tags = $this->_tags->fetchAll();
    }
    
    public function editAction ()
    {
        // get the identifier
        $id_article = $this->_request->getParam('id_article');
        
        $this->view->form = $form = new Form_Articles();
        
        // populate form
        if (!empty($id_article)) {
            $form->populate($this->_articles->fetchById($id_article)->toArray());
        }
    }
}