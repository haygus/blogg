<?php

class Admin_ArticlesController extends App_Controller_Admin
{
    /**
     * Le service Article
     * @var Service_Articles
     */
    protected $_articles;
    
    /**
     * Init pour l'UI de l'admin
     */
    public function initAdmin()
    {
        $this->_articles = Service_Articles::getInstance();
    }

    public function indexAction()
    {
        // collection articles
        $this->view->articles = $this->_articles->fetchAll();
    }
    
    public function editAction ()
    {
        // get the identifier
        $id_article = $this->_request->getParam('id_article');
        
        $this->view->form = $form = new Form_Articles();
        
        // populate form
        if (!empty($id_article)) {
            $form->populate($this->_articles->fetchById($id_article)->toArray());
        }
    }
}