<?php

class Admin_IndexController extends App_Controller_Admin
{

    public function initAdmin()
    {
        /* Initialize action controller here */
    	
    }

    public function indexAction()
    {
    	
    }

    public function loginAction()
    {
    	// Si on est connecté on redirige vers l'admin
    	if (Zend_Auth::getInstance()->hasIdentity()) {
    		$this->_redirect('admin');
    		exit();
    	}
    	// assign the form
        $this->view->form = $form = new Form_Login;
        $form->setAction($this->view->url(array(), 'admin_login'));
        
        
        if($post = $this->_request->isPost()){
        	// on récupère le post
            $formData = $this->getRequest()->getPost();
            
            // on récupère le nom de la table des Users
            $usersTable = new Model_DbTable_Users;
            $usersTableName = $usersTable->getTableName();
            
            // authentifier
            $auth = new App_Auth($usersTableName, 'login', 'password');

            // On ajoute le validateur
            $form->getElement('password')->addValidator(new Validator_Login($formData['nickname'], $auth));
            
            // Connexion autorisée !
            if($form->isValid($formData)){
                
                $user = Zend_Auth::getInstance()->getIdentity();
                
                // redirection
                $this->_redirect('admin');
                exit();
                
            }else{
                $form->populate($formData);
            }
        }
    }
    
}
