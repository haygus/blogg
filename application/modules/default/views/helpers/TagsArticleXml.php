<?php
/**
 * Helper qui retourne les tags à partir de la collection pour le RSS
 */
class Helper_TagsArticleXml extends Zend_View_Helper_Abstract
{
    public function TagsArticleXml(Model_Collection_Tags $tags, Zend_View $view)
    {
        
        $tagsArray = array();
        foreach ($tags as $tag) {
            $tagsArray[] = $tag->label;
        }
        
        $str = implode(', ', $tagsArray);
        
        
        return $str;
               
    }
}