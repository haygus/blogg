<?php
/**
 * Helper qui retourne les tags à partir de la collection
 */
class Helper_TagsArticle extends Zend_View_Helper_Abstract
{
    public function TagsArticle(Model_Collection_Tags $tags, Zend_View $view)
    {
        $str = '[';
        
        $tagsArray = array();
        foreach ($tags as $tag) {
            $tagsArray[] = '<a href="'.$view->baseUrl().'/tag/'.urlencode($tag->label).'" xml:lang="fr" lang="fr" rel="tag">'.$tag->label.'</a>';
        }
        
        $str .= implode('] [', $tagsArray);
        $str .= ']';
        
        return $str;
               
    }
}