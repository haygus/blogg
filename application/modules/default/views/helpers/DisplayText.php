<?php
/**
 * Helper qui parse le texte d'un article et qui le transforme en conséquence
 */
class Helper_DisplayText extends Zend_View_Helper_Abstract
{
    public function DisplayText($string)
    {
        // On remplace les #µ_ par des tags
        $str = preg_replace_callback('|\#µ_(.+)_µ\#|Uu', 'Helper_DisplayText::callbackRegex', $string);;
        
        // coloration syntaxique du code
        $str = str_replace('<code>', '<pre class="prettyprint"><code>', $str);
        $str = str_replace('</code>', '</code></pre>', $str);
        
        
        return $str;
    }
    
    public static function callbackRegex($matches)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        return '[<a href="'.$view->baseUrl().'/tag/'.urlencode($matches[1]).'" rel="tag" xml:lang="fr" lang="fr">'.$matches[1].'</a>]';
    }
    
}