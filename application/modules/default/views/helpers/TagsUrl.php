<?php
/**
 * Helper qui retourne les tags à partir de la collection
 */
class Helper_TagsUrl extends Zend_View_Helper_Abstract
{
    public function TagsUrl(Model_Collection_Tags $tags)
    {
        $str = '[';
        
        $tagsArray = array();
        foreach ($tags as $tag) {
            $tagsArray[] = $tag->label;
        }
        
        $str .= implode('] [', $tagsArray);
        $str .= ']';
        
        return $str;
               
    }
}