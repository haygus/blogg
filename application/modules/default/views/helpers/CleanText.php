<?php
/**
 * Nettoie le text pour le flux RSS
 */
class Helper_CleanText extends Zend_View_Helper_Abstract
{
    public function CleanText($string)
    {
        $str = '';
        
        // On remplace les #µ_ par des tags
        $str = preg_replace('|\#µ_(.+)_µ\#|Uu', '$1', $string);
        
        $str = strip_tags($str);
        
        if (strlen($str) > 300) {
            // on coupe au 1er espace à partir de 300
            $pos = strpos(substr($str, 300), ' ');

            if ($pos === FALSE) {
                $pos = 0;
            }
            $str = substr($str, 0, 300 + $pos);
            
            $str .= '...';
        }
        return $str;
    }
}