<?php

class TagController extends Zend_Controller_Action
{
    /**
     * The tag
     * @var string
     */
    protected $_tag;

    public function init()
    {
        // param extérieur
        $this->_tag = $this->getRequest()->getParam('tag');
        
        $this->view->tag = $this->_tag;
    }

    public function indexAction()
    {
        // service article
        $oArticles = Service_Articles::getInstance();

        $tags = array();
        
        if (!empty($this->_tag)) $tags = array($this->_tag);
        // collection URLs
        $this->view->articles = $oArticles->fetchByTags($tags);
        
        
        
    }

}
