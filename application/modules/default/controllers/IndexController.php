<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
    }
    
    /**
     * Home action
     */
    public function indexAction()
    {
        $oArticles = Service_Articles::getInstance();
        
        // collection articles
        $this->view->articles = $oArticles->fetchAll(array('online' => '1'));
        
   }
    
    /**
     * RSS action
     *  
     */
    public function rssAction()
    {
        $oArticles = Service_Articles::getInstance();
        
        // collection articles
        $this->view->articles = $oArticles->fetchAll();
        
        $this->getHelper('Layout')->disableLayout();
   }


    /**
     * Side box action 
     */
    public function urlAction()
    {
        // number of urls dsplayed
        $displayedUrls = 15;
        
        // get the service
        $oUrls = Service_Urls::getInstance();

        $tag = $this->getRequest()->getParam('tag', '');
        
        if (!empty($tag)) {
            $this->view->tag = $tag;
            
            // collection URLs tagged
            $this->view->urls = $oUrls->fetchAll(0, $displayedUrls, $tag);
        } else {
            // collection URLs
            $this->view->urls = $oUrls->fetchAll(0, $displayedUrls);
        }
    }
    
    // show the post
    public function articleAction()
    {
    	// get token from url
        $token = $this->getRequest()->getParam('token');
        
        $oArticle = Service_Articles::getInstance();
        
        // is the token valid ?
        try {
            $this->view->article = $oArticle->fetchByToken($token);
        } catch (App_Model_Mapper_RuntimeException $e) {
            
            // post not found - error 404
            $this->getResponse()->setHttpResponseCode(404);
            
            // Sortie nouvelle exception
            throw new Zend_Controller_Action_Exception($e->getMessage());
        }
    }
    
    public function licenceAction()
    {
    	
    }
}
