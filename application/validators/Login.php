<?php
/**
 * Validator for login action 
 * 
 * @author haygus
 *
 */
class Validator_Login extends Zend_Validate_Abstract
{
	/**
	 * Constante générée par l'erreur
	 * @var string
	 */
    const LOGIN_FAILED = 'loginFailed';

    /**
     * Nickname de l'utilisateur
     * 
     * @var string
     */
    protected $_compare;
    
	/**
	 * L'instance contenant l'authentifier
	 * @var App_Auth
	 */
    protected $_auth;
    
    /**
     * Tableau des erreurs
     * @var array
     */
    protected $_messageTemplates = array(
    		self::LOGIN_FAILED => "Couple login/password incorrect."
    );
    
    /**
     * Constructeur, il assigne les attributs
     * 
     * @param string $compare
     * @param App_Auth $auth
     */
    public function __construct($compare, App_Auth $auth)
    {
        $this->_compare = $compare;
        $this->_auth = $auth;
    }

    /**
     * Vérifie avec App_Auth que le login/password est OK
     * 
     * @see Zend_Validate_Interface::isValid()
     */
    public function isValid($value)
    {
        $this->_setValue((string) $value);
        
        if (!$this->_auth->authenticate($this->_compare, $value))  {
            $this->_error(self::LOGIN_FAILED);
            return false;
        }
        return true;
    }
}