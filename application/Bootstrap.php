<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initAutoload()
    {
    	$autoloader = new Zend_Application_Module_Autoloader(array(
                    'namespace' => '',
                    'basePath' => dirname(__FILE__),
            ));
            /* On ajoute des namespaces pour l'autoload */
            $namespaces = array(
                'objects' => array(
                    'namespace' => 'Model_Object',
                    'path'      => 'models/objects',
                ),
                'collection' => array(
                    'namespace' => 'Model_Collection',
                    'path'      => 'models/collections',
                ),
                'actionHelper' => array(
                    'namespace' => 'Helper',
                    'path'      => 'helpers',
                ),
                'validators' => array(
                    'namespace' => 'Validator',
                    'path'      => 'validators',
                ),
            );
            $autoloader->addResourceTypes($namespaces);
            
            return $autoloader;
    }
    /**
     * Set the adapter for the datamapper
     *
     * @return void
     */
    protected function _initDataMapper()
    {
        $this->bootstrap('db');
        
        App_Model_Mapper_MapperAbstract::setDefaultAdapter(
            $this->getPluginResource('db')->getDbAdapter()
        );
    }
    
    /**
     * Setup router configuration
     * 
     * @return Zend_Controller_Router_Rewrite
     */
    protected function _initRouting()
    {
        $router = $this->getPluginResource('router')->getRouter();
        $configRouter = new Zend_Config(include dirname(__FILE__) . '/configs/routes.config.php');
        $router->addConfig($configRouter);
        $router->removeDefaultRoutes();

        return $router;
    }
}