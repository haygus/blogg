<?php

return array(
    'tagFilter' => array(
        'type'  => 'Zend_Controller_Router_Route',
        'route' => 'tag/:tag',
        'defaults' => array(
            'module'     => 'default',
            'controller' => 'tag',
            'action'     => 'index'
        )
    ),
    'article' => array(
        'type'  => 'Zend_Controller_Router_Route',
        'route' => 'article/:token',
        'defaults' => array(
            'module'     => 'default',
            'controller' => 'index',
            'action'     => 'article'
        )
    ),
    'license' => array(
        'type' => 'Zend_Controller_Router_Route_Static',
        'route' => 'licence',
        'defaults' => array(
            'module' => 'default',
            'controller' => 'index',
            'action' => 'licence'
        )
    ),
    
    'rss' => array(
        'type' => 'Zend_Controller_Router_Route_Static',
        'route' => 'feed',
        'defaults' => array(
            'module' => 'default',
            'controller' => 'index',
            'action' => 'rss'
        )
    ),
    
    
    
    /*********************************
     **************ADMIN**************
     *********************************/
    'admin' => array(
        'type' => 'Zend_Controller_Router_Route',
        'route' => 'admin/',
        'defaults' => array(
            'module' => 'admin',
            'controller' => 'index',
            'action'     => 'index'
        )
    ),
    'admin_articles' => array(
    		'type' => 'Zend_Controller_Router_Route',
    		'route' => 'admin/articles/:action/*',
    		'defaults' => array(
    				'module' => 'admin',
    				'controller' => 'articles',
    				'action' => 'index'
    		)
    ),
    'admin_tags' => array(
    		'type' => 'Zend_Controller_Router_Route',
    		'route' => 'admin/tags/:action/*',
    		'defaults' => array(
    				'module' => 'admin',
    				'controller' => 'tags',
    				'action' => 'index'
    		)
    ),
    'admin_login' => array(
        'type' => 'Zend_Controller_Router_Route_Static',
        'route' => 'admin/login',
        'defaults' => array(
            'module' => 'admin',
            'controller' => 'index',
            'action' => 'login'
        )
    ),
    /***********************************
     ********DEFAULT ROUTE**************
     ***********************************/
    'defaultRoute' => array(
        'type'  => 'Zend_Controller_Router_Route_Static',
        'route' => '',
        'defaults' => array(
            'module'     => 'default',
            'controller' => 'index',
            'action'     => 'index'
        )
    ),  
    
);