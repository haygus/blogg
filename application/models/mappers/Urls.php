<?php

/**
 * Mapper des Urls
 */

class Model_Mapper_Urls 
    extends App_Model_Mapper_MapperAbstract
    implements App_Model_Mapper_MapperInterface
{

    /**
     * Instance de l'objet BDD des URLs
     * @var Model_DbTable_Urls
     * @access protected
     */
    protected $_DbTableUrls;
    
    protected function _init()
    {
        $this->_DbTableUrls = new Model_DbTable_Urls();
    }
    
    /** 
     * Fetch all URLs
     */
    public function fetchAll(array $options)
    {
        $select = $this->_adapter
                ->select()
                ->from (array('u' => $this->_DbTableUrls->getTableName()),
                        array('u.*'))
                ->order('u.date_lastmod DESC');
        
        // options
        if (isset($options['offset']) && isset($options['limit'])) {
            $select->limit($options['limit'], $options['offset']);
        }
        
        // tag options
        if (isset($options['tag'])) {
            $select->joinLeft(array('tu' => 'hayg_tags_urls'), 
                                    'u.id_url = tu.id_url',
                                    array())
                    ->joinLeft(array('t' => 'hayg_tags'), 
                                    'tu.id_tag = t.id_tag',
                                    array())
                    ;
            $select->where('t.label = ?', $options['tag']);
        }
        $urls = $this->_adapter->fetchAll($select);
        
        return $urls;
    }
}