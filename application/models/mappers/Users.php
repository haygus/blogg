<?php

/**
 * Mapper des Users
 */

class Model_Mapper_Users 
    extends App_Model_Mapper_MapperAbstract
    implements App_Model_Mapper_MapperInterface
{
    /**
     * Instance de l'objet BDD des Users
     * @var Model_DbTable_Users
     */
    protected $_DbTableUsers;
    
    /**
     * Instancie la table des utilisateurs
     * @see App_Model_Mapper_MapperAbstract::_init()
     */
    protected function _init()
    {
        $this->_DbTableUsers = new Model_DbTable_Users();
    }
    
    /**
     * Fetch all users
     * 
     * @see App_Model_Mapper_MapperInterface::fetchAll()
     */ 
     
    public function fetchAll(array $options)
    {
        // build query
        $select = $this->_adapter
                ->select()
                ->from (array('u' => $this->_DbTableUsers->getTableName()),
                        array('u.*'))
                ->order('u.id_user DESC');
        
        $users = $this->_adapter->fetchAll($select);
        
        return $users;
    }
    
    /**
     * Fetch users by Tags
     * @param array tags
     * @return array
     */
    public function fetchByTags(array $tags)
    {
        // Si c'est un tableau vide
        if (empty($tags)) $tags = array(0);
        
        // build the query
        $select = $this->_adapter
                ->select()
                ->from(array('u' => $this->_DbTableUsers->getTableName()),
                        array('u.*'))
                ->join(array('tu' => 'hayg_tags_users'),
                        'tu.id_user = u.id_user',
                        array())
                ->where('ta.id_tag IN(?)', $tags);
        
        return $this->_adapter->fetchAll($select);
    }
}