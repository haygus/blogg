<?php

/**
 * Mapper des Articles
 */

class Model_Mapper_Articles 
    extends App_Model_Mapper_MapperAbstract
    implements App_Model_Mapper_MapperInterface
{

    /**
     * Instance de l'objet BDD des Articles
     * @var Model_DbTable_Articles
     */
    protected $_DbTableArticles;
    
    /**
     * On instancie la base de données des articles
     * @see App_Model_Mapper_MapperAbstract::_init()
     */
    protected function _init()
    {
        $this->_DbTableArticles = new Model_DbTable_Articles();
    }
    
    /**
     * Fetch posts
     * 
     * @see App_Model_Mapper_MapperInterface::fetchAll()
     */
    public function fetchAll(array $options)
    {
    	// build the query
        $select = $this->_adapter
                ->select()
                ->from (array('a' => $this->_DbTableArticles->getTableName()),
                        array('a.*'))
                ->order('a.date_published DESC');
        
        // online for front
        if (isset($options['online'])) {
            $select->where('`online` = ?', $options['online']);
        }
        
        // fetch posts
        $articles = $this->_adapter->fetchAll($select);
        return $articles;
    }
    
    /**
     * Fetch a post by a token
     * @param string token
     * @return array
     */
    public function fetchByToken($token)
    {
    	// build the query
        $select = $this->_adapter
                ->select()
                ->from (array('a' => $this->_DbTableArticles->getTableName()),
                        array('a.*'))
                ->order('a.date_published DESC')
                ->where('a.`token` = ?', $token);
        
        // fetch one post
        $article = $this->_adapter->fetchRow($select);
        return $article;
    }
    
    /**
     * Fetch post by Tags
     * 
     * @param array tags
     * @return array
     */
    public function fetchByTags(array $tags)
    {
        // Si c'est un tableau vide
        if (empty($tags)) $tags = array(0);
        
        // build the query
        $select = $this->_adapter
                ->select()
                ->from(array('a' => $this->_DbTableArticles->getTableName()),
                        array('a.*'))
                ->join(array('ta' => 'hayg_tags_articles'),
                        'ta.id_article = a.id_article',
                        array())
                ->order('a.date_published DESC')
                ->where('ta.id_tag IN(?)', $tags);
        
        return $this->_adapter->fetchAll($select);
    }
    
    /**
    * Fetch a post by id_article
    * @param int $id_article
    * @return array
    */
    public function fetchById($id_article)
    {
    	// build the query
    	$select = $this->_adapter
    	->select()
    	->from (array('a' => $this->_DbTableArticles->getTableName()),
    			array('a.*'))
    			->where('a.`id_article` = ?', $id_article);
    	// get this articles
    	$article = $this->_adapter->fetchRow($select);

    	return $article;
    }
}