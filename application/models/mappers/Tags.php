<?php

/**
 * Mapper des Urls
 */

class Model_Mapper_Tags 
    extends App_Model_Mapper_MapperAbstract
    implements App_Model_Mapper_MapperInterface
{
    
    /**
     * Instance de l'objet BDD des TAGs
     * @var Model_DbTable_Tags
     * @access protected
     */
    protected $_DbTableTags;

    /**
     * Tableau d'instance des objets reliés aux tags
     * @var array
     * @access protected
     */
    protected $_DbTableRelateds = array();
    
    protected function _init()
    {
        $this->_DbTableTags = new Model_DbTable_Tags();   
    }
    
    /** 
     * Fetch all Tags
     */
    public function fetchAll(array $options)
    {
        $select = $this->_adapter
                ->select()
                ->from (array('t' => $this->_DbTableTags->getTableName()),
                        array('t.*'));
        
        $select->order('t.date_lastmod DESC');
        
        $tags = $this->_adapter->fetchAll($select);
        
        return $tags;
    }
    
    /**
     * Méthode générique qui va renvoyer tous les tags reliés
     * @param array Les clé primaires des données liées
     * @param string Le nom de l'objet Dbtable à instancier
     * @access public
     * return array
     */
    public function fetchByRelated($relateds, $DbTableName)
    {
        
        // @see App_Model_DbTable_DbTableAbstract
        $DbTable = $this->_getRelatedDbTable($DbTableName);
        
        // On crée un tableau avec juste les clé primaire
        $ids = $this->_buildPrimaryArray($relateds, $DbTable);
        
        // Query
        $select = $this->_adapter
                ->select()
                ->from (array('t' => $this->_DbTableTags->getTableName()),
                        array('t.*'))
                ->join(array('xt' => $DbTable->getTableName()),
                        't.'.$this->_DbTableTags->getPrimaryKey().' = xt.'.$this->_DbTableTags->getPrimaryKey(),
                        '');
        
        if (isset($ids) && is_array($ids) && count($ids) > 0) {
            
            // On ajoute la condition
            $select->where('`xt`.`'.$DbTable->getRelatedKey().'` IN (?)', $ids);
        }
        
        $select->order('t.date_lastmod DESC');
        
        $datas = $this->_adapter->fetchAll($select);
        
        return $datas;
        
    }
    
    /**
     * Retourne un tableau avec les clés primaire
     * @param array $datas
     * @param App_Model_DbTable_DbTableAbstract $DbTable
     * @return type array
     */
    protected function _buildPrimaryArray($datas, $DbTable)
    {
        
        $ids = array();
        foreach ($datas as $data) {
            $ids[] = $data[$DbTable->getRelatedKey()];
        }
        
        return $ids;
    }
    
    /**
     * Retourne un objet App_Model_DbTable_DbTableAbstract 
     * Instancie App_Model_DbTable_DbTableAbstract s'il n'a pas été instancié auparavant
     * @param string $DbTableName 
     * @return App_Model_DbTable_DbTableAbstract
     */
    protected function _getRelatedDbTable($DbTableName)
    {
        // On vérifie si on a déjà instancier dans ce mapper la DbTable demandé
        if (!array_key_exists($DbTableName, $this->_DbTableRelateds)) {
            $DbTable = $this->_DbTableRelated[$DbTableName] = new $DbTableName;
        } else {
            // pour faire court
            $DbTable = $this->_DbTableRelated[$DbTableName];
        }
        
        return $DbTable;
    }
    
    /**
     * Retourne les liaisons
     * @access public
     * @param array $relateds 
     * @param string $DbTableName
     * @return array
     */
    public function getTagsByRelateds($relateds, $DbTableName)
    {
        // @see App_Model_DbTable_DbTableAbstract
        $DbTable = $this->_getRelatedDbTable($DbTableName);
        
        // On crée un tableau avec juste les clé primaire
        $ids = $this->_buildPrimaryArray($relateds, $DbTable);
        
        $select = $this->_adapter
                ->select()
                ->from (array('xt' => $DbTable->getTableName()),
                        array('xt.*'));
                
                
        if (isset($ids) && is_array($ids) && count($ids) > 0) {
            // On ajoute la condition
            $select->where('`xt`.`'.$DbTable->getRelatedKey().'` IN (?)', $ids);
        }
        
        return $this->_adapter->fetchAll($select);
        
    }
    
    /**
     * Créer les liens entre les tags et les entités
     * @param array $relateds
     * @param string $DbTableName 
     * @param Model_Collection_Tags $tags
     * @param array $tagsRelateds
     */
    public function buildRelatedsLinks($relateds, $DbTableName, Model_Collection_Tags $tags, $tagsRelateds)
    {
        // @see App_Model_DbTable_DbTableAbstract
        $DbTable = $this->_getRelatedDbTable($DbTableName);
        
        $tagged = array();
        
        // On boucle sur les URLS recups
        foreach ($relateds as &$related) {
            
            // On boucle sur les liaisons
            foreach ($tagsRelateds as $tagsRelated) {
                // c'est le bon tag
                if ($related[$DbTable->getRelatedKey()] == $tagsRelated[$DbTable->getRelatedKey()]) {
                    $tagged[$related[$DbTable->getRelatedKey()]][$tagsRelated[$this->_DbTableTags->getPrimaryKey()]] = $tagsRelated;
                }
            }
            
            $taggedCollection = null;
            
            // On boucle sur les tags
            foreach ($tags as $tag) {
                
                // Si ce tag est avec cet URL
                if (array_key_exists($tag->{$this->_DbTableTags->getPrimaryKey()}, $tagged[$related[$DbTable->getRelatedKey()]])) {
                    // SI l'objet est déjà crée on ne fait rien
                    if ($taggedCollection instanceof Model_Collection_Tags) {
                    // on instancie l'objet
                    } else {
                        $taggedCollection = new Model_Collection_Tags('Model_Object_Tags');
                    }
                    // On ajout à la collection le tag
                    $taggedCollection->offsetSet($tag->{$this->_DbTableTags->getPrimaryKey()}, $tag);
                    
                }
            }
            // On ajout la collection Tags aux URLs
            $related['tags'] = $taggedCollection;
        }
        
        return $relateds;
    }
    
    /**
     * Récupère les tags par le labels
     * @param array $tags 
     * return array
     */
    public function fetchByLabels(array $tags)
    {
        $select = $this->_adapter
                ->select()
                ->from (array('t' => $this->_DbTableTags->getTableName()), array('id_tag'));
                
        // On ajoute la condition
        $select->where('`t`.`label` IN (?)', $tags);
            
        return $this->_adapter->fetchAll($select);
    }
}