<?php

class Model_DbTable_Urls extends App_Model_DbTable_DbTableAbstract
{
    /**
     * Le nom de la table URLs
     * @var string
     */
    protected $_name = 'hayg_urls';
    
    /**
     * La clé primaire
     * @var string
     */
    protected $_primary = 'id_url';

}