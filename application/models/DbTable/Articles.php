<?php

class Model_DbTable_Articles extends App_Model_DbTable_DbTableAbstract
{
    /**
     * Le nom de la table URLs
     * @var string
     */
    protected $_name = 'hayg_articles';
    
    /**
     * La clé primaire
     * @var string
     */
    protected $_primary = 'id_article';

}