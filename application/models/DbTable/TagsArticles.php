<?php

class Model_DbTable_TagsArticles extends App_Model_DbTable_DbTableAbstract
{
    /**
     * Le nom de la table de liaisons URLs et TAGs
     * @var string
     */
    protected $_name = 'hayg_tags_articles';
    
    
    /**
     * La clé qui est à la donnée taguée
     * @var string
     */
    
    protected $_relatedKey = 'id_article';
}