<?php

class Model_DbTable_TagsUrls extends App_Model_DbTable_DbTableAbstract
{
    /**
     * Le nom de la table de liaisons URLs et TAGs
     * @var string
     */
    protected $_name = 'hayg_tags_urls';
    
    
    /**
     * La clé qui est à la donnée taguée
     * @var string
     */
    
    protected $_relatedKey = 'id_url';
}