<?php

class Model_DbTable_Users extends App_Model_DbTable_DbTableAbstract
{
    /**
     * Le nom de la table des users
     * @var string
     */
    protected $_name = 'hayg_users';
    
    /**
     * La clé primaire
     * @var string
     */
    protected $_primary = 'id_user';

}