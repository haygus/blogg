<?php
class Model_DbTable_Tags extends App_Model_DbTable_DbTableAbstract
{

    /**
     * Le nom de la table TAGs
     * @var string
     * @access protected
     */

    protected $_name = 'hayg_tags';
    
    
    /**
     * La clé primaire
     * @var string
     * @access protected
     */
    protected $_primary = 'id_tag';
}
