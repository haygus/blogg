<?php

/**
 * Objet métier de l'URL
 */
class Model_Object_Tags extends App_Model_ModelAbstract
{
    /**
     * @see App_Model_ModelAbstract::$_fields
     * @var array
     */
    protected $_fields = array(
        'id_tag',
        'id_parent',
        'label',
        'color',
        'date_lastmod',
        'date_created',
    );
}