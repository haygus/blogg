<?php

/**
 * Objet métier de l'article
 */
class Model_Object_Articles extends App_Model_ModelAbstract
{
    /**
     * @see App_Model_ModelAbstract::$_fields
     * @var array
     */
    protected $_fields = array(
        'id_article',
        'titre',
        'contenu',
        'token',
        'recherche',
        'online',
        'date_published',
        'date_lastmod',
        'date_created',
        'lang',
        'tags',
    );
}