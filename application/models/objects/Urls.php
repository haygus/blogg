<?php

/**
 * Objet métier de l'URL
 */
class Model_Object_Urls extends App_Model_ModelAbstract
{
    /**
     * @see App_Model_ModelAbstract::$_fields
     * @var array
     */
    protected $_fields = array(
        'id_url',
        'label',
        'description',
        'url',
        'date_lastmod',
        'date_created',
        'lang',
        'tags',
    );
}