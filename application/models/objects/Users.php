<?php

/**
 * Objet métier d'un user
 */
class Model_Object_Users extends App_Model_ModelAbstract
{
    /**
     * @see App_Model_ModelAbstract::$_fields
     * @var array
     */
    protected $_fields = array(
        'id_user',
        'nom',
        'email',
        'site_web',
        'twitter',
        'login',
        'password',
        'droits',
        'date_lastmod',
        'date_created',
        'tags',
    );
}