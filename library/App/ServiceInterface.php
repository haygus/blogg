<?php

/**
 * Classe abstraite des Service
 */

interface App_ServiceInterface
{
    /**
     * Fetch all datas
     */
    public function fetchAll();
}