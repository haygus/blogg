<?php
/**
 * Model invalid argument exception
 */
class App_Model_InvalidArgumentException
    extends InvalidArgumentException
    implements App_Model_Exception
{}

