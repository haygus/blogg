<?php
/**
 * Model exception interface
 */
interface App_Model_Exception extends App_Exception
{}