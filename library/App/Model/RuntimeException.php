<?php
/**
 * Model runtime exception
 */
class App_Model_RuntimeException
    extends RuntimeException
    implements App_Model_Exception
{}

