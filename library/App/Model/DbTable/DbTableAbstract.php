<?php
/**
 * @author haygus 
 */
abstract class App_Model_DbTable_DbTableAbstract extends Zend_Db_Table_Abstract
{
    /**
     * Retourne le nom de la table
     * @access public
     * @return string
     */
    public function getTableName()
    {
        return $this->_name;
    }
    
    /**
     * Retourne la clé primaire
     * @access public
     * @return string
     */
    public function getPrimaryKey()
    {
        if (isset($this->_primary) && !empty($this->_primary)) {
            return $this->_primary;
        } else {
            throw new Zend_Db_Table_Exception('No primary Key');
        }
    }
    
    /**
     * Retourne la clé qui est taguée
     * @access public
     * @return string
     */
    public function getRelatedKey() {
        if (isset($this->_relatedKey) && !empty($this->_relatedKey)) {
            return $this->_relatedKey;
        } else {
            throw new Zend_Db_Table_Exception('No related Key');
        }
    }   
}