<?php
/**
 * Mapper runtime exception
 * @author DASPRiD
 */
class App_Model_Mapper_RuntimeException
    extends RuntimeException
    implements App_Model_Mapper_Exception
{}
