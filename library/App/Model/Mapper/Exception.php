<?php
/**
 * Mapper exception interface
 */
interface App_Model_Mapper_Exception extends App_Model_Exception
{}