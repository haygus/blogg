<?php
/**
 * Abstract datamapper
 * @author DASPRiD
 */
abstract class App_Model_Mapper_MapperAbstract
{
    /**
     * Default adapter
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected static $_defaultAdapter;

    /**
     * Actual adapter to use
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_adapter;

    /**
     * Instantiate a new mapper with a specific adapter.
     *
     * If no adapter is defined, the default adapter is used. If there is also
     * no default adapter, an exception is thrown.
     *
     * @param  Zend_Db_Adapter_Abstract $adapter
     * @throws App_Mapper_RuntimeException When no adapter was defined
     * @throws
     */
    public function __construct(Zend_Db_Adapter_Abstract $adapter = null)
    {
        if ($adapter === null) {
            $adapter = self::getDefaultAdapter();
        }

        if ($adapter === null) {
            throw new App_Model_Mapper_RuntimeException('No adapter was defined');
        }

        $this->_adapter = $adapter;

        $this->_init();
    }

    /**
     * Do some initial stuff
     *
     * @return void
     */
    protected function _init()
    {}

    /**
     * Get the adapter
     *
     * @return Zend_Db_Adapter_Abstract
     */
    public static function getDefaultAdapter()
    {
        return self::$_defaultAdapter;
    }

    /**
     * Set the adapter
     *
     * @param  Zend_Db_Adapter_Abstract $adapter
     * @return void
     */
    public static function setDefaultAdapter(Zend_Db_Adapter_Abstract $adapter)
    {
        self::$_defaultAdapter = $adapter;
    }
}
