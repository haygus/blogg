<?php
/**
 * @author haygus 
 */
interface App_Model_Mapper_MapperInterface {
    
	/**
	 * build query and fetch datas
	 * 
	 * @param array $options
	 * @return array
	 */
    public function fetchAll(array $options);
}