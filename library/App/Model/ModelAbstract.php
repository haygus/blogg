<?php
/**
 * Abstract domain model
 * @author DASPRiD
 */
abstract class App_Model_ModelAbstract
{
    /**
     * Allowed fields in this entity
     *
     * @var array
     */
    protected $_fields = array();

    /**
     * List of field values
     *
     * @var array
     */
    protected $_values = array();

    /**
     * Create an new instance of this domain model
     *
     * @param array $values
     */
    public function __construct(array $values)
    {
        foreach ($values as $name => $value) {
            $this->$name = $value;
        }
    }

    /**
     * Set a field
     *
     * @param  string $name
     * @param  mixed  $value
     * @throws App_Model_OutOfBoundsException When field does not exist
     * @return void
     */
    public function __set($name, $value)
    {
        $method = '_set' . ucfirst($name);

        if (method_exists($this, $method)) {
            $this->{$method}($value);
        } else if (in_array($name, $this->_fields)) {
            $this->_values[$name] = $value;
        } else {
            throw new App_Model_OutOfBoundsException('Field with name ' . $name . ' does not exist');
        }
    }

    /**
     * Get a field
     *
     * @param  string $name
     * @throws App_Model_OutOfBoundsException When field does not exist
     * @return mixed
     */
    public function __get($name)
    {
        $method = '_get' . ucfirst($name);

        if (method_exists($this, $method)) {
            return $this->{$method}();
        } else if (in_array($name, $this->_fields)) {
            if (!array_key_exists($name, $this->_values)) {
                throw new App_Model_RuntimeException('Trying to accessing field ' . $name . ' which value was not set yet');
            }

            return $this->_values[$name];
        } else {
            throw new App_Model_OutOfBoundsException('Field with name ' . $name . ' does not exist');
        }
    }
    
    /**
     * Return the array with values
     * @param bool $deeper
     * @return array
     */
    public function toArray($deeper = false)
    {
        // the return array
        $return = array();
        
        // is the array is available
        if (!isset($this->_fields) || !is_array($this->_fields)) {
            throw new App_Model_RuntimeException('Model objct not ready to be transform as an array');
        }
        
        // populate our array
        foreach ($this->_fields as $field) {
            $return[$field] = '';
            if (isset($this->_values[$field])) {
                if (is_scalar($this->_values[$field]) || ($deeper)) {
                    $return[$field] = $this->_values[$field];
                }
            }
        }
        
        return $return;
    }
}
