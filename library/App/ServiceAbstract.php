<?php

abstract class App_ServiceAbstract 
    extends App_Singleton
    implements App_ServiceInterface
{
    
    /**
     * Le nom de la classe de la collection
     * @var type string
     */
    protected $_collectionClass = 'App_Model_CollectionAbstract';
    
    
    /**
     * L'instance de la classe collection
     * @var type App_Model_CollectionAbstract
     */
    protected $_collection;
    
    /**
     * Allowed domain class in this collection
     *
     * @var string
     */
    protected $_domainClass;
    
    /**
     * Le mapper
     * @access protected
     * @var App_Model_Mapper_MapperAbstract
     */
    protected $_mapper;
    
    /**
     * Construit un objet Métier URL avec le résultat de la BDD
     * @access protected
     * @param $values array
     * @return Model_Object_Urls
     */
    protected function _createObject(array $values)
    {
        return new $this->_domainClass($values);
    }
    
    /**
     * Construit une collection qui contient tous les objets métier URLs dont on a besoin
     * @param array $results
     * @return App_Model_CollectionAbstract
     */
    
    protected function _createCollection(array $results)
    {
        $this->_collection = new $this->_collectionClass($this->_domainClass);
        
        foreach ($results as $values) {
            // On l'ajoute à la collection
            $this->_collection->offsetSet(null, $this->_createObject($values));
            
        }
        
        return $this->_collection;
    }
    
    /**
     * get the collection cllass name
     * @return string
     */
    public function getCollectionClass()
    {
        return $this->_collectionClass;
    }
    
    /**
     * Get the mapper
     * @return App_Model_Mapper_MapperAbstract
     */
    public function getMapper()
    {
        return $this->_mapper;
    }
    
}

