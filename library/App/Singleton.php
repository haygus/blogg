<?php


abstract class App_Singleton {
    
    /**
     * Les instances, cette classe est un singleton
     * 
     * @staticvar
     * @var array 
     */
    private static $instances = array();
    
    /** 
     * executes init method
     * 
     * @final
     */
    final private function __construct()
    {
        if(method_exists($this, 'init')) {
            $this->init();
        }
    }
    
    /**
     * bloc any clone
     * 
     * @final
     */
    final public function __clone()
    {
        trigger_error("Le clonage n'est pas autorisé.", E_USER_ERROR);
    }

    /**
     * get the singleton instance
     * 
     * @final
     * @static
     */
    final public static function getInstance()
    {
        $c = get_called_class();
        
        if(!isset(self::$instances[$c])) {
            self::$instances[$c] = new $c;
        }
        return self::$instances[$c];
    }
}