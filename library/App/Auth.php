<?php

class App_Auth
{
    /**
     * Instance de Zend_Auth
     * @var Zend_Auth
     */
    private $_auth;
    
    /**
     * La table
     * @æccess protected 
     * @var string
     */
    protected $_name;
    
    /**
     * le champ du login
     * @var string
     */
    protected $_identityColumn;
    
    /**
     * le champs du password
     * @var string
     */
    protected $_passwordColumn;
    
    /**
     * Stockage de l'identité
     * @var Zend_Auth_Storage_Session
     */
    protected $_storage;
    
    /**
     * Constructeur
     * @param string $tableName
     * @param string $identityColumn
     * @param string $passwordColumn 
     */
    
    public function __construct($tableName, $identityColumn, $passwordColumn) {
        
        $this->_name = $tableName;
        $this->_identityColumn = $identityColumn;
        $this->_passwordColumn = $passwordColumn;
        
        $this->_auth = Zend_Auth::getInstance();
    }
    
    /**
     * @param string $user
     * @param string $password
     * @access public
     * @return bool
     */
    public function authenticate($user, $password)
    {
        $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_Db_Table::getDefaultAdapter(),
                $this->_name,
                $this->_identityColumn,
                $this->_passwordColumn);
        
        $authAdapter->setIdentity($user)
                    ->setCredential(App_String::HashPassword($password));
        
        $result = $authAdapter->authenticate();
        
        if ($result->isValid()) {
            
            // On stocke l'identité
            $this->_storage = $storage = $this->_auth->getStorage();
            
            $storage->write(new Model_Object_Users((array) $authAdapter->getResultRowObject(
                null,
                'password'
            )));
            
        }
        return $result->isValid();
    }
}