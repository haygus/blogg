<?php
/**
 * Classe qui fait des traitements sur des chaînes de caractères
 * @author haygus
 *
 */
class App_String {
    /**
     * Prend le mot passe brut et le transforme
     * @param string $string,
     * @param integer $len = 32 
     * @param string $hash = 'sha1'
     * @return string
     */
    static public function HashPassword($string, $len = 32, $hash = 'sha1')
    {

        $lenLess = $len * -1;

        $newString = hash($hash, $string);
        
        $aThird = floor($len / 3);
        $half = floor($len / 2);
        $aTenth = floor($len / 10);
        
        $beforeString = mb_substr($string, 0, $aTenth);
        $afterString = mb_substr($string, -($aTenth));

        $beforeHash = mb_substr(hash($hash, $beforeString), 0, $aThird);
        $afterHash = mb_substr(hash($hash, $afterString), 0, $aThird);
        $newString = substr_replace($newString,$beforeHash, 0, $aThird);
        $newString = substr_replace($newString,$afterHash, $half, $aThird);

        $newString = mb_substr($newString, $lenLess, $len);
        return $newString;
    }

    /**
     * Coupe le texte en trop
     * @param string $string
     * @param integer $len = 20
     * @param string $ellipse = '...'
     * @return string
     */
    static public function truncate($string, $len = 20, $ellipse = '...')
    {
        $newString = substr($string, 0, $len);

        if (strlen($string) > $len) {
                $newString = $newString.$ellipse;
        }
        return $newString;
    }


    /**
     * Vire tous les caractères spéciaux
     * @param string $string 
     * 
     * @return string
     */
    static public function specialChars($texte)
    {
        $texte = str_replace(array("è","é","ê","ë","È","É","Ê","Ë"),"e",$texte);
        $texte = str_replace(array("à","á","â","ã","ä","À","Á","Â","Ã","Ä"),"a",$texte);
        $texte = str_replace(array("ò","ó","ô","õ","ö","Ò","Ó","Ô","Õ","Ö"),"o",$texte);
        $texte = str_replace(array("ì","í","î","ï","Ì","Í","Î","Ï"),"i",$texte);
        $texte = str_replace(array("ù","ú","û","ü","Ù","Ú","Û","Ü"),"u",$texte);
        $texte = str_replace(array("ñ","Ñ"),"n",$texte);
        $texte = str_replace(array("ç","Ç"),"c",$texte);
        $texte = str_replace(array("ý","ÿ","Ý"),"y",$texte);
        $texte = str_replace("_","-",$texte);
        $texte = strtolower($texte);
        
        //Suppression des espaces et caracteres spéciaux
        $texte = preg_replace('#([^a-z0-9-])#','-',$texte);
        
        //Suppression des tirets multiples
        $texte = preg_replace('#([-]+)#','-',$texte);
        
        // pas de - au debut et a la fin de l'url
        // Suppression du premier caractère si c'est un tiret
        if(!empty($texte{0}) && $texte{0} == '-') {
            $texte = substr($texte,1);
        }
        
        //Suppression du dernier caractère si c'est un tiret
        if(substr($texte, -1, 1) == '-') {
        	$texte = substr($texte, 0, -1);
        }
        return $texte;
    }
}