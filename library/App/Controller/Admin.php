<?php
/**
 * Classe controller qui vérifie qu'on a bien accès à l'admin 
 */
class App_Controller_Admin extends Zend_Controller_Action
{
    final public function init()
    {
        // init method for admin actions
        if (method_exists($this, 'initAdmin')) {
            $this->initAdmin();
        }
        // not connected
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            // redirect if not the action is login
            if ($this->_request->getActionName() != 'login') {
                    // back to home
                $this->_redirect('');
            }
        }
    }
}