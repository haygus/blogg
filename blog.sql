-- phpMyAdmin SQL Dump
-- version 3.4.5deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Ven 11 Mai 2012 à 11:18
-- Version du serveur: 5.1.62
-- Version de PHP: 5.3.6-13ubuntu3.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données: `haygus`
--

-- --------------------------------------------------------

--
-- Structure de la table `hayg_articles`
--

CREATE TABLE IF NOT EXISTS `hayg_articles` (
  `id_article` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `contenu` text NOT NULL,
  `token` varchar(255) NOT NULL,
  `recherche` text NOT NULL,
  `online` tinyint(1) NOT NULL,
  `date_published` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_lastmod` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `token` (`token`),
  KEY `id_article` (`id_article`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `hayg_articles`
--

INSERT INTO `hayg_articles` (`id_article`, `titre`, `contenu`, `token`, `recherche`, `online`, `date_published`, `date_lastmod`, `date_created`) VALUES
(2, 'Megaupload est mort! Vive le peer to peer!', '<p>Comme vous avez du le lire, MegaUpload (et ses frères) sont hors-ligne, le FBI a lancé <a href="http://www.scribd.com/doc/78786408/Mega-Indictment" rel="external">un mandat d''arrêt international</a> contre 7 personnes (le haut de MegaUpload) et 2 entreprises (MEGAUPLOAD LIMIT et VESTOR LIMITED).\r\n</p>La société est basée à Hong Kong mais des serveurs étaient hébergés en Virginie (E.U), le FBI n''a pas eu de mal à éteindre ces serveurs. Pour le moment on peut oublier MegaUpload, il y aura sûrement des miroirs qui vont apparaître (des faux des vrais du phishing faites bien attention !)</p>\r\n<p>Selon Numerama (<a href="http://www.numerama.com/magazine/21337-le-dossier-accablant-du-procureur-contre-megaupload.html" rel="external">http://www.numerama.com/magazine/21337-le-dossier-accablant-du-procureur-contre-megaupload.html</a>) c''est un travail du FBI qui a commencé l''année dernière.</p>\r\n\r\n<h3>Pas un mal</h3>\r\n\r\n<p>Ce n''est pas grand mal que Megaupload disparaisse c''était un très gros service centralisé, simple à censurer : une seule cible.</p>\r\n\r\n<h3>La suite ?</h3>\r\n\r\n<h4>Direct <abbr title="download">DL</abbr></h4>\r\n<p>Pour le direct <abbr title="download">DL</abbr>, pas de problèmes juste tous les liens vers megaupload mènent vers des 404 (page non trouvée), il faut re-héberger tous ça à différents endroits.<p>\r\n\r\n<h4>Le partage</h4>\r\n\r\n<p>Qu''on se mette d''accord, megaupload n''était pas un site de "#µ_partage_µ#" en tant que tel, oui on pouvait mettre ses photos de vacances et donner le lien à ses amis, mais on passait (comme vous avez pu le voir) par une plate-forme qui se prenait une commission publicitaire sur vos 40 secondes de temps de cerveau disponible.</p>\r\n<p>Le #µ_partage_µ# c''est un échange entre 2 personnes physique non-commercial, le protocole - presque parfait - qui correspond à cet échange est le #µ_peer to peer_µ# il est (presque) décentralisé les fichiers sont enregistrés à plusieurs endroits, il est plus difficile de censurer ces fichiers. Si vous voulez partagez vos photos utilisez le <abbr title="peer to peer">P2P</abbr>.</p>\r\n\r\n\r\n<h3>Conclusion</h3>\r\n<p>Donc, Internet s''affaiblit de plus en plus, il faut défendre le réseau pour qu''on puisse conservé nos liberté, pour certains l''#µ_infowar_µ# ( ou World War Web ou cyber-guerre choisissez ) a commencé.</p>\r\n<p>Des sites ont été ciblés par les #µ_anonymous_µ# : </p>\r\n<ul>\r\n	<li>Justice.gov</li>\r\n    <li>RIAA.org</li>\r\n    <li>MPAA.org</li>\r\n    <li>UniversalMusic.com</li>\r\n    <li>Hadopi.fr</li>\r\n</ul>\r\n\r\n<p>Sources : </p>\r\n<ul>\r\n	<li><a href="http://www.numerama.com/magazine/21335-megaupload-la-cyberguerre-a-commence.html" rel="external">http://www.numerama.com/magazine/21335-megaupload-la-cyberguerre-a-commence.html</a></li>\r\n	<li><a href="http://pixellibre.net/2012/01/megaupload-nest-plus-19-01-2012/" rel="external">http://pixellibre.net/2012/01/megaupload-nest-plus-19-01-2012/</a></li>\r\n	<li><a href="http://korben.info/megaupload-ferme.html" rel="external">http://korben.info/megaupload-ferme.html</a>\r\n	<li><a href="http://www.numerama.com/magazine/21337-le-dossier-accablant-du-procureur-contre-megaupload.html" rel="external">http://www.numerama.com/magazine/21337-le-dossier-accablant-du-procureur-contre-megaupload.html</a></li>\r\n</lu>\r\n', 'megaupload-est-mort-vive-le-peer-to-peer', '', 1, '2012-01-20 13:00:00', '2012-01-20 13:00:00', '2012-01-20 13:00:00'),
(1, 'Mon nouveau blog en ligne !', '<p>Après quelques lignes de code #µ_PHP_µ# (et #µ_Zend Framework_µ#) mon blog perso est en ligne !</p>\r\n\r\n<h3>Présentation</h3>\r\n<p>Je me présente, mon surnom/pseudo est "haygus" (je ne suis pas anonyme il est simple de récupèrer mon nom/prenom), je suis un (jeune) développeur web #µ_PHP_µ# défenseur des logiciels #µ_libre_µ#, je travaille avec GNU/Linux, contribue (très peu - pour l''instant) aux logiciels libres.</p>\r\n<p>Vous pouvez me contacter par e-mail à cette adresse blog[_at_]haygus.fr</p>\r\n<h3>Le blog</h3>\r\n<p>Blog, par définition tout qui peut me m''interesser, #µ_PHP_µ#, #µ_liberté_µ#, #µ_démocratie_µ#, #µ_hacking_µ#, et pleins d''autres choses. </p>\r\n<h4>Wall of Links</h4>\r\n<p>Il y a pour l''instant seulement le bloc dynamique à gauche "Wall of links" (si vous avez mieux je prends) qui me permet de partager des articles/pages/sites intéressant qui vaut le coup d''oeil.</p>\r\n<h4>Tags</h4>\r\n<p>Ce blog est centré autour d''une chose : les Tags, il y a en aura partout, on pourra naviguer avec seulement certains tags (imaginez, vous aurez un filtre sur TOUS le blog avec un ou plusieurs tags (en passant par les articles|liens|commentaires?)</p>\r\n<h4>Techniques</h4>\r\n<p>Comme dit plus haut ce blog est développé avec le langage server #µ_PHP_µ#, utilise le #µ_Zend Framework_µ#, tourne avec le server web #µ_Apache_µ# sur une #µ_Debian_µ# #µ_Squeeze_µ#.\r\nPour l''instant ce blog est sur bitbucket sur un repos privé, il est prévu que je partage le code source sous une license libre plus tard, j''ai encore d''autres fonctionnalités(bug ?) de prévues.\r\n</p>', 'mon-nouveau-blog-en-ligne', '', 0, '2012-01-13 15:30:00', '2012-01-13 15:30:00', '2012-01-13 15:30:00');

-- --------------------------------------------------------

--
-- Structure de la table `hayg_articles_users`
--

CREATE TABLE IF NOT EXISTS `hayg_articles_users` (
  `id_article` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  KEY `hayg_articles_users_ibfk_1` (`id_article`),
  KEY `hayg_articles_users_ibfk_2` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `hayg_tags`
--

CREATE TABLE IF NOT EXISTS `hayg_tags` (
  `id_tag` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `color` varchar(6) NOT NULL,
  `date_lastmod` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_tag`),
  UNIQUE KEY `label` (`label`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Contenu de la table `hayg_tags`
--

INSERT INTO `hayg_tags` (`id_tag`, `id_parent`, `label`, `color`, `date_lastmod`, `date_created`) VALUES
(1, 0, 'développement', '800080', '2011-12-29 00:06:00', '2011-12-29 00:00:20'),
(2, 0, 'révolution', '000000', '2011-12-30 00:00:00', '2011-12-30 00:00:00'),
(3, 0, 'hacking', '103d1c', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 0, 'réseau', '003399', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 4, 'télécom', '333366', '2012-01-12 00:00:01', '2012-01-12 00:00:01'),
(6, 4, 'neutralité du réseau', '000CCC', '2012-01-12 00:00:05', '2012-01-12 00:00:05'),
(7, 8, 'Zend Framework', '57a900', '2012-01-12 00:00:10', '2012-01-12 00:00:10'),
(8, 1, 'PHP', '9999CC', '2012-01-13 00:00:00', '2012-01-13 00:00:00'),
(9, 0, 'partage', '999999', '2012-01-20 03:11:02', '2012-01-20 03:11:02'),
(10, 0, 'peer to peer', 'BBBBBB', '2012-01-20 03:11:02', '2012-01-20 03:11:02'),
(11, 0, 'anonymous', '111111', '2012-01-20 03:11:02', '2012-01-20 03:11:02'),
(12, 0, 'infowar', '121415', '2012-01-20 03:11:02', '2012-01-20 03:11:02');

-- --------------------------------------------------------

--
-- Structure de la table `hayg_tags_articles`
--

CREATE TABLE IF NOT EXISTS `hayg_tags_articles` (
  `id_tag` int(10) unsigned NOT NULL,
  `id_article` int(10) unsigned NOT NULL,
  KEY `id_tag` (`id_tag`),
  KEY `id_article` (`id_article`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `hayg_tags_articles`
--

INSERT INTO `hayg_tags_articles` (`id_tag`, `id_article`) VALUES
(7, 1),
(8, 1),
(2, 1),
(9, 2),
(10, 2),
(11, 2),
(12, 2);

-- --------------------------------------------------------

--
-- Structure de la table `hayg_tags_urls`
--

CREATE TABLE IF NOT EXISTS `hayg_tags_urls` (
  `id_tag` int(10) unsigned NOT NULL,
  `id_url` int(10) unsigned NOT NULL,
  KEY `id_tag` (`id_tag`),
  KEY `id_url` (`id_url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `hayg_tags_urls`
--

INSERT INTO `hayg_tags_urls` (`id_tag`, `id_url`) VALUES
(1, 1),
(2, 2),
(3, 2),
(5, 3),
(6, 3),
(7, 4),
(9, 5),
(10, 5),
(12, 6);

-- --------------------------------------------------------

--
-- Structure de la table `hayg_tags_users`
--

CREATE TABLE IF NOT EXISTS `hayg_tags_users` (
  `id_tag` int(10) unsigned NOT NULL,
  `id_user` int(10) unsigned NOT NULL,
  KEY `hayg_tags_users_ibfk_1` (`id_tag`),
  KEY `hayg_tags_users_ibfk_2` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `hayg_urls`
--

CREATE TABLE IF NOT EXISTS `hayg_urls` (
  `id_url` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(500) NOT NULL,
  `lang` enum('fr','en') NOT NULL,
  `date_lastmod` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `hayg_urls`
--

INSERT INTO `hayg_urls` (`id_url`, `label`, `description`, `url`, `lang`, `date_lastmod`, `date_created`) VALUES
(1, 'Bases: retour aux sources de la programmation objet', 'Bases: retour aux sources de la programmation objet', 'http://code.anonymation.com/architecture/bases-retour-aux-sources-de-la-programmation-objet/', 'fr', '2011-12-29 06:08:02', '2011-12-29 01:00:03'),
(2, 'Telecomix crée la mémoire vive des révolutions', 'Telecomix crée la mémoire vive des révolutions', 'https://owni.fr/2011/12/30/telecomix-cree-la-memoire-vive-des-revolutions/', 'fr', '2011-12-30 00:00:00', '2011-12-30 00:00:00'),
(3, 'Les télécoms sans autorité', 'Les télécoms sans autorité', 'http://owni.fr/2012/01/12/les-telecoms-sans-autorite/', 'fr', '2012-01-12 00:00:00', '2012-01-12 00:00:00'),
(4, 'Module Bootstraps in Zend Framework: Do''s and Don''ts', 'Module Bootstraps in Zend Framework: Do''s and Don''ts', 'http://mwop.net/blog/234-Module-Bootstraps-in-Zend-Framework-Dos-and-Donts', 'en', '2012-01-12 00:00:10', '2012-01-12 00:00:10'),
(5, 'Megaupload est mort : nous entrons dans une ère nouvelle', 'Megaupload est mort : nous entrons dans une ère nouvelle', 'http://reflets.info/megaupload-est-mort-nous-entrons-dans-une-ere-nouvelle', 'fr', '2012-01-20 03:11:02', '2012-01-20 03:11:02'),
(6, 'World War Web : Bougez pas… on va vous montrer à quoi ça pourrait ressembler', 'World War Web : Bougez pas… on va vous montrer à quoi ça pourrait ressembler', 'http://reflets.info/world-war-web-bougez-pas-on-va-vous-montrer-a-quoi-ca-pourrait-ressembler/', 'fr', '2012-01-25 00:00:00', '2012-01-25 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `hayg_users`
--

CREATE TABLE IF NOT EXISTS `hayg_users` (
  `id_user` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `site_web` varchar(500) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `droits` enum('reader','writer','admin','super-admin') NOT NULL DEFAULT 'writer',
  `date_lastmod` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `hayg_users`
--

INSERT INTO `hayg_users` (`id_user`, `nom`, `email`, `site_web`, `twitter`, `login`, `password`, `droits`, `date_lastmod`, `date_created`) VALUES
(1, 'Haygus', 'blog@haygus.fr', 'www.haygus.fr', 'https://twitter.com/haygus', 'haygus', 'blob', 'super-admin', '2012-01-10 00:00:00', '2012-01-10 00:00:00');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `hayg_articles_users`
--
ALTER TABLE `hayg_articles_users`
  ADD CONSTRAINT `hayg_articles_users_ibfk_1` FOREIGN KEY (`id_article`) REFERENCES `hayg_articles` (`id_article`) ON UPDATE CASCADE,
  ADD CONSTRAINT `hayg_articles_users_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `hayg_users` (`id_user`);

--
-- Contraintes pour la table `hayg_tags_articles`
--
ALTER TABLE `hayg_tags_articles`
  ADD CONSTRAINT `hayg_tags_articles_ibfk_1` FOREIGN KEY (`id_tag`) REFERENCES `hayg_tags` (`id_tag`) ON UPDATE CASCADE,
  ADD CONSTRAINT `hayg_tags_articles_ibfk_2` FOREIGN KEY (`id_article`) REFERENCES `hayg_articles` (`id_article`);

--
-- Contraintes pour la table `hayg_tags_urls`
--
ALTER TABLE `hayg_tags_urls`
  ADD CONSTRAINT `hayg_tags_urls_ibfk_1` FOREIGN KEY (`id_tag`) REFERENCES `hayg_tags` (`id_tag`) ON UPDATE CASCADE,
  ADD CONSTRAINT `hayg_tags_urls_ibfk_2` FOREIGN KEY (`id_url`) REFERENCES `hayg_urls` (`id_url`);

--
-- Contraintes pour la table `hayg_tags_users`
--
ALTER TABLE `hayg_tags_users`
  ADD CONSTRAINT `hayg_tags_users_ibfk_1` FOREIGN KEY (`id_tag`) REFERENCES `hayg_tags` (`id_tag`) ON UPDATE CASCADE,
  ADD CONSTRAINT `hayg_tags_users_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `hayg_users` (`id_user`);

